@extends('mainpage')
@section('content')

  <!-- section detail -->
  <section class="section-detail">
    <div class="container flex-column">
      <h1 class="subtext-title">Detail Produk</h1>
      <div class="content d-flex flex-column flex-lg-row justify-content-between align-items-start">
        <div class="content-left">
          <img src="{{url('')}}/{{$product->image}}" alt="" class="img img-detail">
        </div>
        <div class="content-center description d-flex flex-column">
          <h4 class="description-title">{{ $product->name_produk }}</h4>
          <div class="description-rating d-flex align-items-center">
            <div class="description-rating-wrap d-flex align-items-center">
              <div class="description-star">
                @php
                    $rating = $product->rating;
                    $fullStars = floor($rating);
                    $halfStar = ceil($rating - $fullStars);
                    $emptyStars = 5 - $fullStars - $halfStar;
                @endphp
            
                {{-- Full stars --}}
                @for ($i = 0; $i < $fullStars; $i++)
                    <img src="{{ asset('assets/images/ic_star-shop.png') }}" alt="" class="ic">
                @endfor
            
                {{-- Half star --}}
                @if ($halfStar)
                    <img src="{{ asset('assets/images/ic_star-shop.png') }}" alt="" class="ic">
                @endif
            
                {{-- Empty stars --}}
                @for ($i = 0; $i < $emptyStars; $i++)
                    <img src="{{ asset('assets/images/ic_star-shop.png') }}" alt="" class="ic">
                @endfor
              </div>
              <span class="description-rating-span">({{ $product->rating }})</span>
            </div>
            <span class="description-totaly">({{ $product->sold }}) terjual</span>
          </div>
            @if ($product->shop)
              <span class="description-span">{{ $product->shop->name_shop }}</span>
            @else
              <span class="description-span">Uncategorized</span>
            @endif
          <h5 class="description-price">({{ $product->product_price }})</h5>
          <div class="description-text d-flex flex-column">
            <h5 class="description-text-title">Deskripsi</h5>
            @if($product->category)
            <p class="description-text-desc">({{ $product->detail }})
            @else
            <p class="description-text-desc">Uncategorized</p>
            @endif
            </p>
          </div>
        </div>
        <div class="content-right count d-flex flex-column">
          <h5 class="count-title">Jumlah</h5>
          <div class="count-wrap d-flex justify-content-between align-items-center">
              <span class="minus">-</span>
              <span class="count-text">1</span>
              <span class="plus">+</span>
          </div>
          <span class="totaly-title">Total Harga</span>
          <h5 id="total-price" class="totaly-price">Rp.{{ $product->product_price }}</h5>
          <a href="{{ $product->shop->phone_number }}" class="btn btn-primary">Pesan</a>
        </div>
      </div>
    </div>
  </section>

  <section class="section-populer detail-page">
    <div class="container content">
        <div class="content-top">
            <h3 class="title">Produk Lainnya</h3>
        </div>
        <div class="content-bottom">
            @foreach ($related_products as $related_product)
                <div class="card">
                    <img src="{{ url($related_product->image) }}" alt="" class="img card-image">
                    <div class="card-body">
                        @if ($related_product->category)
                            <span class="subtitle">{{ $related_product->category->name_category }}</span>
                        @else
                            <span class="subtitle">Uncategorized</span>
                        @endif
                        <h4 class="title"><a href="{{ route('product.showDetail', ['product' => $related_product->id]) }}" style="color: black">{{ $related_product->name_produk }}</a></h4>
                        <div class="rating">
                            <div class="wrap-star">
                              @php
                              $rating = $related_product->rating;
                              $fullStars = floor($rating);
                              $halfStar = ceil($rating - $fullStars);
                              $emptyStars = 5 - $fullStars - $halfStar;
                              @endphp
                          
                              {{-- Full stars --}}
                              @for ($i = 0; $i < $fullStars; $i++)
                                  <img src="{{ asset('assets/images/ic_star-shop.png') }}" alt="" class="ic">
                              @endfor
                          
                              {{-- Half star --}}
                              @if ($halfStar)
                                  <img src="{{ asset('assets/images/ic_star-shop.png') }}" alt="" class="ic">
                              @endif
                          
                              {{-- Empty stars --}}
                              @for ($i = 0; $i < $emptyStars; $i++)
                                  <img src="{{ asset('assets/images/ic_star-shop.png') }}" alt="" class="ic">
                              @endfor
                            </div>
                            <span>({{ $related_product->rating }})</span>
                        </div>
                        <span class="subtitle">{{ $related_product->shop->name_shop }}</span>
                        <div class="price">
                            <h4 class="price-text">Rp.{{ number_format($related_product->product_price) }}</h4>
                            <a href="{{ $related_product->shop->phone_number }}" class="btn btn-price">Pesan</a>
                        </div>
                    </div>
                </div>
            @endforeach
        </div>
    </div>
  </section>
  <script>
    document.addEventListener("DOMContentLoaded", function() {
        // Get elements
        var countText = document.querySelector('.count-text');
        var totalyPrice = document.getElementById('total-price');
        var minusButton = document.querySelector('.minus');
        var plusButton = document.querySelector('.plus');

        var productPrice = parseFloat("{{ $product->product_price }}");
        var quantity = parseInt(countText.textContent);

        function updateTotalPrice() {
            var totalPrice = productPrice * quantity;
            totalyPrice.textContent = "Rp." + totalPrice.toFixed(2);
        }

        minusButton.addEventListener('click', function() {
            if (quantity > 1) {
                quantity--;
                countText.textContent = quantity;
                updateTotalPrice();
            }
        });

        plusButton.addEventListener('click', function() {
            quantity++;
            countText.textContent = quantity;
            updateTotalPrice();
        });
    });
  </script>
@endsection