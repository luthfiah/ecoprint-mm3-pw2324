<!-- resources/views/pages/form-category.blade.php -->

@extends('mainpage')
@section('content')

<section class="section-populer toko-page">
    <div class="container content">
        <div class="content-top">
            <h3 class="title">Category</h3>
            <a href="{{ route('viewCategory.formAddCategory') }}" class="btn btn-primary">Tambah</a>
        </div>
        @if(Session::has('success'))
            <div class="alert alert-success w-100" role="alert">
                {{ Session::get('success') }}
            </div>
        @endif
        <div class="contents-bottom">
            @if($categories && count($categories) > 0)
                <table>
                    <tr>
                        <th>Jenis Category</th>
                        <th>Gambar</th>
                        <th>Option</th>
                    </tr>
                    @foreach($categories as $category)
                        <tr>
                            <td>{{ $category->name_category }}</td>
                            <td><img src="{{ asset($category->image) }}" alt="" class="img card-image"></td>
                            <td class="option">
                                <a href="{{ route('viewCategory.formEditCategory', ['category' => $category->id]) }}" class="btn btn-secondary">Edit</a>

                                <form action="{{ route('category.destroy',['category'=>$category->id]) }}" method="POST" class="">
                                    @method('DELETE')
                                    @csrf
                                    <button type="submit" class="btn btn-secondary option-button" >Delete</button>
                                </form>
                            </td>
                        </tr>
                    @endforeach
                </table>
            @else
                <p>No categories available.</p>
            @endif
        </div>
    </div>
</section>

@endsection
