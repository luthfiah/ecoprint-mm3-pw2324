@extends('mainpage')
@section('content')

  <!-- search -->
  <section class="section-search">
    <div class="container">
      <div class="wrap-desc">
        <img src="{{ asset('assets/images/ic_resources.png') }}" alt="" class="ic">
        <span>Telusuri semua kategori</span>
      </div>
      <div class="wrap-chart">
        <form action="{{ route('product.search') }}" method="GET">
          <input type="text" placeholder="Cari..." name="query">
          <button type="submit" class="btn btn-search"><img src="{{ asset('assets/images/ic_search.png') }}" alt="" class="ic"></button>
        </form>
      </div>
    </div>
  </section>
  <!-- search end-->

  <section class="section-shop-2_kategori">
    <div class="container">
      <div class="kategori">
        <h4 class="kategori-title"></h4>
        <div class="wrap">
          @foreach($categories as $category)
            <a href="{{ route('category.show', ['category' => $category->id]) }}" class="nav-link">
              <img src="{{ asset($category->image) }}" alt="" class="ic">
              <span class="kategori-text">{{ $category->name_category }}</span>
              

            </a>
          @endforeach
        </div>
      </div>
      <div class="wrapper-card">
        @foreach($products as $product)
          <div class="card">
            <img src="{{ asset($product->image) }}" alt="" class="img card-image">
            <div class="card-body">
              <span class="subtitle">{{ $product->category->name_category }}</span>
              <h4 class="title"><a href="{{ route('product.showDetail', ['product' => $product->id]) }}" style="color: black">{{ $product->name_produk }}</a></h4>
              <div class="rating">
                <div class="wrap-star">
                  @php
                      $rating = $product->rating;
                      $fullStars = floor($rating);
                      $halfStar = ceil($rating - $fullStars);
                      $emptyStars = 5 - $fullStars - $halfStar;
                  @endphp
              
                  {{-- Full stars --}}
                  @for ($i = 0; $i < $fullStars; $i++)
                      <img src="{{ asset('assets/images/ic_star-shop.png') }}" alt="" class="ic">
                  @endfor
              
                  {{-- Half star --}}
                  @if ($halfStar)
                      <img src="{{ asset('assets/images/ic_star-shop.png') }}" alt="" class="ic">
                  @endif
              
                  {{-- Empty stars --}}
                  @for ($i = 0; $i < $emptyStars; $i++)
                      <img src="{{ asset('assets/images/ic_star-shop.png') }}" alt="" class="ic">
                  @endfor
                </div>
                <span>({{ $product->rating }})</span>
              </div>
              <span class="subtitle">{{ $product->shop->name_shop }}</span>
              <div class="price">
                <h4 class="price-text">Rp.{{ number_format($product->product_price) }}</h4>
                <span class="price-span">{{ $product->sold }} terjual</span>
              </div>
            </div>
          </div>
        @endforeach
      </div>
    </div>
  </section>
  @endsection