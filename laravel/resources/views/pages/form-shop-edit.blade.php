@extends('mainpage')
@section('content')

<!-- input product -->
<section class="section-input-product">
    <div class="container flex-column w-100 justify-content-center align-items-center">
        <h1 class="input-product-title">Edit Toko</h1>
        @if(Session::has('success'))
        <div class="alert alert-success w-100" role="alert">
            {{ Session::get('success') }}
        </div>
        @endif
        <form action="{{ route('viewShop.editShop', ['shop' => $shop->id]) }}" method="POST">
            @csrf
            @method('PATCH')
            <div class="wrap-input-product">
                <label for="fname">Nama Toko</label>
                <input type="text" id="fname" placeholder="Masukan nama toko" name="fname"
                    value="{{ old('fname') ?? $shop->name_shop }}">
                @error('fname')
                <div class="text-danger">{{ $message }}</div>
                @enderror
            </div>
            <div class="wrap-input-product">
                <label for="fphone">Link Whatsapp</label>
                <input type="text" id="fphone" placeholder="Masukan link whatsapp" name="fphone"
                    value="{{ old('fphone') ?? $shop->phone_number }}">
                @error('fphone')
                <div class="text-danger">{{ $message }}</div>
                @enderror
            </div>
            <input type="submit" class="btn btn-primary">
        </form>
    </div>
</section>
<!-- input product -->

@endsection
