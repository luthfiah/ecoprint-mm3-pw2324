@extends('mainpage')
@section('content')

  <!-- input product -->
  <section class="section-input-product">
    <div class="container flex-column w-100 justify-content-center align-items-center">
      <h1 class="input-product-title">Tambah Product</h1>
      @if(Session::has('success'))
        <div class="alert alert-success w-100" role="alert">
            {{ Session::get('success') }}
        </div>
        @endif
      <form action="{{route('viewProduct.addProduct')}}" method="post" enctype="multipart/form-data">
        @csrf
        <div class="wrap-input-product">
          <label for="">Nama Produk</label>
          <input type="text" id="fname" placeholder="Masukan nama produk" name="fname">
          @error('fname')
            <div  class="text-danger">{{ $message }}</div>
          @enderror
        </div>
        <div class="subtext-input">
          <h6 class="subtext-input-title">Toko</h6>
          <select class="form-select" id="shop" name="shop" aria-label="Pilih toko">
              @foreach ($shops as $shop)
                  <option value="{{ $shop->id }}">{{ $shop->name_shop }}</option>
              @endforeach
          </select>
          <span class="span-text">Pilih toko produk Anda!</span>
        </div>
        <div class="wrap-input-product">
          <label for="">Harga Produk</label>
          <input type="number" id="product_price" placeholder="Masukan harga product" name="product_price">
          @error('product_price')
            <div  class="text-danger">{{ $message }}</div>
          @enderror
        </div>
        <div class="wrap-input-product">
          <label for="">Gambar Produk</label>
          <input type="file" id="image" name="image">
          @error('image')
            <div  class="text-danger">{{ $message }}</div>
          @enderror
        </div>
        <div class="wrap-input-product">
          <label for="">Detail Produk</label>
          <textarea id="detail" placeholder="Masukkan detail produk" name="detail"></textarea>
          @error('detail')
            <div  class="text-danger">{{ $message }}</div>
          @enderror
        </div>
        <div class="wrap-input-product">
          <label for="">Jumlah Produk</label>
          <input type="number" id="sold" placeholder="Masukan jumlah terjual" name="sold">
          @error('sold')
            <div  class="text-danger">{{ $message }}</div>
          @enderror
        </div>
        <div class="wrap-input-product">
          <label for="">Rating Produk</label>
          <input type="text" id="rating" placeholder="Masukan rating" name="rating">
          @error('rating')
            <div  class="text-danger">{{ $message }}</div>
          @enderror
        </div>

        <div class="subtext-input">
          <h6 class="subtext-input-title">Kategori</h6>
          <select class="form-select" id="kategori" name="category" aria-label="Pilih Kategori">
              @foreach ($categories as $category)
                  <option value="{{ $category->id }}">{{ $category->name_category }}</option>
              @endforeach
          </select>
          <span class="span-text">Pilih kategori produk Anda!</span>
        </div>
        <input type="submit" class="btn btn-primary">
      </form>
    </div>
  </section>
  <!-- input product -->

@endsection