@extends('mainpage')
@section('content')

  <!-- header -->
  <div class="section-manfaat-header">
    <div class="container content">
      <div class="content-left">
        <img src="{{ asset('assets/images/manfaat-img-header.png') }}" alt="" class="img">

      </div>
      <div class="content-right subtext">
        <div class="subtext-text">
          <span class="subtitle">Tentang Ecoprint</span>
          <h3 class="title">Apakah Ecoprint? Mengungkap Keindahan Mode yang Ramah Lingkungan</h3>
        </div>
        <div class="subtext-text">
          <p class="desc">
            Ecoprint adalah teknik pewarnaan media seperti kain, kertas, atau kulit dengan menggunakan bahan alam
            seperti daun,
            bunga, dan batang. Prosesnya melibatkan kontak langsung antara bahan alam dan media, di mana pigmen warna
            dilepaskan dan
            menempel pada permukaan kain.
            <br><br>
            Prinsip pembuatan ecoprint adalah melalui kontak langsung antara bahan alam dengan media kain. Bahan alam
            yang digunakan
            akan melepaskan pigmen warnanya dan menempel pada kain.
            <br><br>
            Tujuan ecoprint adalah untuk menghasilkan karya seni yang unik dan otentik, meningkatkan kesadaran
            masyarakat akan
            pentingnya kelestarian alam, dan menciptakan lapangan pekerjaan dan meningkatkan perekonomian masyarakat.

          </p>
        </div>
      </div>
    </div>
  </div>
  <!-- header -->

  <!-- manfaat -->
  <section class="section-manfaat page-manfaat_manfaat">
    <div class="container container-manfaat content">
      <div class="content-top subtext">
        <h2 class="subtext-display">Sustainable Beauty, Menggali <span class="text-third">Manfaat</span> Ecoprint untuk
          Gaya <span class="text-third">Hidup Berkelanjutan</span></h2>
        <p class="subtext-desc">Ecoprint bukan hanya tentang gaya, tetapi juga tentang membangun masa depan yang
          berkelanjutan. Dengan bahan ramah
          lingkungan dan proses pencetakan yang bertanggung jawab, Ecoprint mempersembahkan gaya yang bermakna untuk
          gaya hidup
          Anda dan lingkungan.</p>
      </div>
      <div class="content-bottom items">
        @foreach ($benefits as $benefit)
          <div class="item">
            <img src="{{ $benefit['image'] }}" alt="" class="img">
            <div class="item-text">
              <h4 class="item-text-title">{{ $benefit['title'] }}</h4>
              <p class="item-text-desc">{{ $benefit['detail'] }}</p>
            </div>
          </div>
        @endforeach
      </div>
    </div>
  </section>
  <!-- manfaat end -->
  @endsection