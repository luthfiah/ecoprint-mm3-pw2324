@extends('mainpage')
@section('content')

  <!-- input product -->
  <section class="section-input-product">
    <div class="container flex-column w-100 justify-content-center align-items-center">
      <h1 class="input-product-title">Tambah Category</h1>
      @if(Session::has('success'))
        <div class="alert alert-success w-100" role="alert">
        {{Session::get('success')}}
        </div>
      @endif
      <form action="{{route('viewCategory.addCategory')}}" method="POST" enctype="multipart/form-data">
        @csrf
        <div class="wrap-input-product">
          <label for="">Nama Category</label>
          <input type="text" id="fname" placeholder="Masukan nama category" name="fname">
          @error('fname')
            <div  class="text-danger">{{ $message }}</div>
          @enderror
        </div>
        <div class="wrap-input-product">
          <label for="">Gambar Kategori</label>
          <input type="file" id="image" name="image">
          @error('image')
            <div  class="text-danger">{{ $message }}</div>
          @enderror
        </div>
        <input type="submit" class="btn btn-primary">
      </form>
    </div>
  </section>
  <!-- input product -->

  @endsection