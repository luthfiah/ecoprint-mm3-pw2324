<!-- resources/views/pages/form-toko.blade.php -->

@extends('mainpage')
@section('content')

<section class="section-populer toko-page">
    <div class="container content">
        <div class="content-top">
            <h3 class="title">Toko</h3>
            <a href="{{ route('viewShop.formAddShop') }}" class="btn btn-primary">Tambah</a>
        </div>
        @if(Session::has('success'))
            <div class="alert alert-success w-100" role="alert">
                {{Session::get('success')}}
            </div>
        @endif
        <div class="contents-bottom">
            @if($shops && count($shops) > 0)
                <table>
                    <tr>
                        <th>Id Toko</th>
                        <th>Nama Toko</th>
                        <th>Link Whatsapp</th>
                        <th>Option</th>
                    </tr>
                    @foreach($shops as $shop)
                        <tr>
                            <td>{{ $shop->id }}</td>
                            <td>
                                <a href="{{ route('viewProduct', ['shop_id' => $shop->id]) }}">{{ $shop->name_shop }}</a>
                            </td>
                            <td>{{ $shop->phone_number }}</td>
                            <td class="option">
                                <a href="{{ route('viewShop.formEditShop', ['shop' => $shop->id]) }}"
                                    class="btn btn-secondary">Edit</a>

                                <form action="{{ route('shop.destroy',['shop'=>$shop->id]) }}" method="POST"
                                    class="option">
                                    @method('DELETE')
                                    @csrf
                                    <button type="submit" class="btn btn-secondary ">Delete</button>
                                </form>
                            </td>
                        </tr>
                    @endforeach
                </table>
            @else
                <p>No shops available.</p>
            @endif
        </div>
    </div>
</section>

@endsection
