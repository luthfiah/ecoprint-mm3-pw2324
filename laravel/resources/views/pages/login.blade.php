@extends('mainpage')
@section('content')
  <section class="section-login">
    <div class="container container-login">
      <div class="subtext">
        <h4 class="subtext-display">Masuk</h4>
        @if(session('pesan'))
          <div class="alert alert-success w-100">
            {{ session('pesan') }}
          </div>
        @endif
        <form action="{{ route('login.process') }}" method="POST">
          @csrf
          <div class="subtext-input">
            <input name="email" type="text" placeholder="Alamat E-mail">
          </div>
          @error('email')
            <div  class="text-danger">{{ $message }}</div>
          @enderror
          <div class="subtext-input">
            <input name="password" type="password" placeholder="Masukkan Password ">
          </div>
          @error('password')
            <div  class="text-danger">{{ $message }}</div>
          @enderror
          <a href="#" class="forget-pass">Lupa Password</a>
          <div class="wrap-button">
            <button type="submit" class="btn btn-primary">Masuk</button>
          </div>
        </form>
        
        {{-- <div class="wrap-button">
          <button class="btn btn-outline"><img src="{{ asset('assets/images/ic-google.svg') }}" alt="">Masuk dengan
            Google</button>
        </div> --}}
        <p class="text-option">Belum punya akun? Ayo<a href="{{ url('/register') }}" class="text-primary"> daftar</a></p>
        </p>
      </div>
    </div>
  </section>

@endsection