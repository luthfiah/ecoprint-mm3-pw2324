@extends('mainpage')
@section('content')


  <!-- section teknik -->
  <section class="section-teknik page-manfaat_teknik">
    <div class="container container-teknik content">
      <div class="content-top subtext">
        <h2 class="subtext-display">Melukis dengan Alam: Memahami <span class="text-third">Teknik</span> dan
          <span class="text-third">Keajaiban</span> di Balik Ecoprint
        </h2>
        <p class="subtext-desc">Ecoprint, seni kreatif yang memanfaatkan keindahan alam untuk menciptakan desain unik
          pada tekstil. Ecoprint
          menghadirkan keajaiban alam ke dalam kain, memadukan keindahan artistik dengan kesadaran akan keberlanjutan.
        </p>
      </div>
      <div class="content-bottom">
        <div class="item">
          <h3 class="item-title">Pounding</h3>
          <div class="item-text">
            <img src="{{ asset('assets/images/teknik-card-1.png') }}" alt="" class="img">
            <ul class="item-text-desc">
              <li><span>Proses:</span> Daun, bunga, atau bagian tumbuhan lain dipukul-pukul di atas kain yang telah
                direndam</li>
              <li><span>Mordant:</span> Larutan mordant digunakan untuk membantu menetapkan warna pada kain.</li>
              <li><span>Hasil:</span> Teknik ini menghasilkan pola yang unik karena bentuk dan tekstur tumbuhan tertanam
                dalam serat kain.</li>
            </ul>
          </div>
        </div>
        <div class="item">
          <h3 class="item-title">Steaming</h3>
          <div class="item-text">
            <img src="{{ asset('assets/images/teknik-card-2.png') }}" alt="" class="img">
            <ul class="item-text-desc">
              <li><span>Proses:</span> Daun, bunga, atau bagian tumbuhan lain diatur di atas kain yang telah direndam
                dalam larutan mordant, kemudian dikukus.</li>
              <li><span>Mordant:</span> Larutan mordant membantu mengikat warna pada serat kain selama proses
                pengukusan.</li>
              <li><span>Hasil:</span> Pengukusan menciptakan transfer warna dan pola alami ke kain.</li>
            </ul>
          </div>
        </div>
        <div class="item">
          <h3 class="item-title">Fermentasi</h3>
          <div class="item-text">
            <img src="{{ asset('assets/images/teknik-card-3.png') }}" alt="" class="img">
            <ul class="item-text-desc">
              <li><span>Proses:</span> Daun, bunga, atau bagian tumbuhan lain dibiarkan terfermentasi selama beberapa
                hari di atas kain yang telah direndam
                dalam larutan mordant.</li>
              <li><span>Mordant:</span> Larutan mordant membantu memperkuat ikatan antara warna dan serat kain.</li>
              <li><span>Hasil:</span> Proses fermentasi memberikan karakteristik unik pada warna dan pola pada kain.
              </li>
            </ul>
          </div>
        </div>
        <div class="item">
          <h3 class="item-title">Iron Blanket</h3>
          <div class="item-text">
            <img src="{{ asset('assets/images/teknik-card-4.png') }}" alt="" class="img">
            <ul class="item-text-desc">
              <li><span>Proses:</span> Daun, bunga, atau bagian tumbuhan lain diletakkan di atas kain yang telah
                direndam dalam larutan mordant, lalu ditutup
                dengan sehelai kain besi.</li>
              <li><span>Mordant:</span> Larutan mordant membantu memastikan perlekatan warna pada kain.</li>
              <li><span>Hasil:</span> Pola dan warna ditransfer ke kain melalui proses oksidasi dengan bantuan panas dan
                logam besi.</li>
            </ul>
          </div>
        </div>
        <div class="item">
          <h3 class="item-title">Bundle Dyeing</h3>
          <div class="item-text">
            <img src="{{ asset('assets/images/teknik-card-5.png') }}" alt="" class="img">
            <ul class="item-text-desc">
              <li><span>Proses:</span> Kain diikat dengan bahan tumbuhan menggunakan tali atau benang sebelum direndam
                dalam larutan mordant.</li>
              <li><span>Mordant:</span> Larutan mordant digunakan untuk memastikan warna menempel pada kain dengan baik.
              </li>
              <li><span>Hasil:</span> Teknik ini menciptakan pola dan variasi warna yang unik di sepanjang kain.</li>
            </ul>
          </div>
        </div>
        <div class="item">
          <h3 class="item-title">Shibori</h3>
          <div class="item-text">
            <img src="{{ asset('assets/images/teknik-card-6.png') }}" alt="" class="img">
            <ul class="item-text-desc">
              <li><span>Proses:</span> Kain dilipat, diikat, atau diberi tekanan sebelum diwarnai, menciptakan pola
                geometris atau abstrak.</li>
              <li><span>Mordant:</span> Larutan mordant membantu menetapkan warna pada kain selama proses pewarnaan.
              </li>
              <li><span>Hasil:</span> Pola yang dihasilkan oleh teknik ini bergantung pada cara kain dilipat dan diikat
                sebelum diwarnai.</li>
            </ul>
          </div>
        </div>
      </div>
    </div>
  </section>
  <!-- section teknik end -->

  @endsection