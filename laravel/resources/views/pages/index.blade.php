@extends('mainpage')
@section('content')

  <!-- section header -->
  <section class="section-home-header">
    <div class="container container-home-header subtext flex-column justify-content-center align-items-center">
      <div class="subtext-text">
        <h1 class="subtext-text-display" data-aos="fade-up">Ecoprint Menawarkan Lebih dari Sekadar <span class="text-secondary">Gaya</span>
        </h1>
        <h3 class="subtext-text-subtitle" data-aos="fade-up" data-aos-delay="300">Mengukir Jejak Positif pada <span class="text-secondary">lingkungan</span>
          dengan Menghargai Alam</h3>
      </div>
      <img src="{{ asset('assets/images/home-img-header.png') }}" alt="" class="img img-home-header" data-aos="zoom-in-up" data-aos-delay="300">
    </div>
  </section>
  <!-- section header end -->

  <!-- section sustinable -->
  <div class="section-sustinable">
    <div class="container container-sustinable justify-content-center">
      <div class="subtext d-flex flex-lg-row flex-column">
        <div class="subtext-image d-flex justify-content-center" data-aos="zoom-in">
          <div class="wrap d-flex flex-column">
            <img src="{{ asset('assets/images/home-subtinable-img-1.png') }}" alt="" class="">
            
            <img src="{{ asset('assets/images/home-subtinable-img-3.png') }}" alt="" class="">
          </div>
          <img src="{{ asset('assets/images/home-subtinable-img-2.png') }}" alt="" class="">
        </div>
        <div class="subtext-text text d-flex flex-column" data-aos="fade-left">
          <div class="text-wrap d-flex flex-column">
            <h4 class="subtext-text-subtitle">Tentang Kami</h4>
            <h3 class="subtext-text-display">Apakah Ecoprint? Mengungkap Keindahan Mode yang Ramah Lingkungan</h3>
          </div>
          <div class="text-wrap d-flex flex-column">
            <p class="subtext-text-desc">Ecoprint adalah teknik pewarnaan media seperti kain, kertas, atau kulit dengan
              menggunakan bahan alam seperti daun,
              bunga, dan batang. Prosesnya melibatkan kontak langsung antara bahan alam dan media, di mana pigmen warna
              dilepaskan dan
              menempel pada permukaan kain.</p>
            <div class="wrap-btn">
              <a href="{{ route('viewBenefit') }}" class="btn btn-link">Baca lebih lanjut <svg xmlns="http://www.w3.org/2000/svg" width="24"
                  height="24" viewBox="0 0 24 24" fill="none">
                  <path
                    d="M14.43 18.8201C14.24 18.8201 14.05 18.7501 13.9 18.6001C13.61 18.3101 13.61 17.8301 13.9 17.5401L19.44 12.0001L13.9 6.46012C13.61 6.17012 13.61 5.69012 13.9 5.40012C14.19 5.11012 14.67 5.11012 14.96 5.40012L21.03 11.4701C21.32 11.7601 21.32 12.2401 21.03 12.5301L14.96 18.6001C14.81 18.7501 14.62 18.8201 14.43 18.8201Z"
                    fill="#887709" />
                  <path
                    d="M20.33 12.75H3.5C3.09 12.75 2.75 12.41 2.75 12C2.75 11.59 3.09 11.25 3.5 11.25H20.33C20.74 11.25 21.08 11.59 21.08 12C21.08 12.41 20.74 12.75 20.33 12.75Z"
                    fill="#887709" />
                </svg></a>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
  <!-- section sustinable end -->

  <!-- section manfaat -->
  <section class="section-manfaat">
    <div class="container container-manfaat content">
      <div class="content-top subtext">
        <h2 class="subtext-display" data-aos="fade-up">Sustainable Beauty, Menggali <span class="text-third">Manfaat</span> Ecoprint untuk
          Gaya <span class="text-third">Hidup Berkelanjutan</span></h2>
        <p class="subtext-desc" data-aos="fade-up" data-aos-delay="300">Ecoprint bukan hanya tentang gaya, tetapi juga tentang membangun masa depan yang
          berkelanjutan. Dengan bahan ramah
          lingkungan dan proses pencetakan yang bertanggung jawab, Ecoprint mempersembahkan gaya yang bermakna untuk
          gaya hidup
          Anda dan lingkungan.</p>
      </div>
      <div class="content-bottom">
        <div class="wrapper">
          <div class="item" data-aos="fade-up" data-aos-delay="300"data-aos-delay="300">
            <span class="item-number">1</span>
            <div class="item-text">
              <h5 class="item-text-title">Ramah lingkungan</h5>
              <p class="item-text-desc">Ecoprint menggunakan bahan alami seperti daun, bunga, dan kayu untuk warna tanpa
                bahan kimia berbahaya sehingga dapat
                menjaga lingkungan bebas limbah beracun.</p>
            </div>
          </div>
          <div class="item" data-aos="fade-up" data-aos-delay="500">
            <span class="item-number">2</span>
            <div class="item-text">
              <h5 class="item-text-title">Kreatif</h5>
              <p class="item-text-desc">Ecoprint menciptakan pola dan warna unik yang tidak dapat diproduksi secara
                massal karena terbentuk secara alami dari
                bahan-bahan alam.</p>
            </div>
          </div>
          <div class="item" data-aos="fade-up" data-aos-delay="700">
            <span class="item-number">3</span>
            <div class="item-text">
              <h5 class="item-text-title">Peluang bisnis</h5>
              <p class="item-text-desc">Ecoprint menjadi peluang bisnis menjanjikan dengan nilai estetika tinggi dan
                ramah lingkungan, sesuai dengan tren saat
                ini.</p>
            </div>
          </div>
        </div>
        <a href="{{ route('viewBenefit') }}" class="btn btn-link btn-link-brown">Baca lebih lanjut <svg xmlns="http://www.w3.org/2000/svg"
            width="24" height="24" viewBox="0 0 24 24" fill="none">
            <path
              d="M14.4301 18.8201C14.2401 18.8201 14.0501 18.7501 13.9001 18.6001C13.6101 18.3101 13.6101 17.8301 13.9001 17.5401L19.4401 12.0001L13.9001 6.46012C13.6101 6.17012 13.6101 5.69012 13.9001 5.40012C14.1901 5.11012 14.6701 5.11012 14.9601 5.40012L21.0301 11.4701C21.3201 11.7601 21.3201 12.2401 21.0301 12.5301L14.9601 18.6001C14.8101 18.7501 14.6201 18.8201 14.4301 18.8201Z"
              fill="#797061" />
            <path
              d="M20.33 12.75H3.5C3.09 12.75 2.75 12.41 2.75 12C2.75 11.59 3.09 11.25 3.5 11.25H20.33C20.74 11.25 21.08 11.59 21.08 12C21.08 12.41 20.74 12.75 20.33 12.75Z"
              fill="#797061" />
          </svg></a>
      </div>
    </div>
  </section>
  <!-- section manfaat end -->

  <!-- section teknik -->
  <section class="section-teknik">
    <div class="container container-teknik content">
      <div class="content-top subtext">
        <h2 class="subtext-display" data-aos="fade-up">Melukis dengan Alam: Memahami <span class="text-third">Teknik</span> dan
          <span class="text-third">Keajaiban</span> di Balik Ecoprint
        </h2>
        <p class="subtext-desc" data-aos="fade-up" data-aos-delay="300">Ecoprint, seni kreatif yang memanfaatkan keindahan alam untuk menciptakan desain unik
          pada tekstil. Ecoprint
          menghadirkan keajaiban alam ke dalam kain, memadukan keindahan artistik dengan kesadaran akan keberlanjutan.
        </p>
      </div>
      <div class="content-bottom" data-aos="fade-up" data-aos-delay="500">
        <div class="card">
          <img src="{{ asset('assets/images/teknik-card-1.png') }}" alt="" class="img card-image">
          <div class="card-text">
            <h4 class="card-text-title">Pounding</h4>
            <p class="card-text-desc">Teknik ecoprint yang dilakukan dengan memukul-mukul daun, bunga, atau bagian
              tumbuhan lain di atas kain yang telah
              direndam dalam larutan mordant.</p>
            <a href="{{ route('viewTechnique') }}" class="btn btn-link">Baca lebih lanjut <svg xmlns="http://www.w3.org/2000/svg" width="24"
                height="24" viewBox="0 0 24 24" fill="none">
                <path
                  d="M14.43 18.8201C14.24 18.8201 14.05 18.7501 13.9 18.6001C13.61 18.3101 13.61 17.8301 13.9 17.5401L19.44 12.0001L13.9 6.46012C13.61 6.17012 13.61 5.69012 13.9 5.40012C14.19 5.11012 14.67 5.11012 14.96 5.40012L21.03 11.4701C21.32 11.7601 21.32 12.2401 21.03 12.5301L14.96 18.6001C14.81 18.7501 14.62 18.8201 14.43 18.8201Z"
                  fill="#887709" />
                <path
                  d="M20.33 12.75H3.5C3.09 12.75 2.75 12.41 2.75 12C2.75 11.59 3.09 11.25 3.5 11.25H20.33C20.74 11.25 21.08 11.59 21.08 12C21.08 12.41 20.74 12.75 20.33 12.75Z"
                  fill="#887709" />
              </svg></a>
          </div>
        </div>
        <div class="card">
          <img src="{{ asset('assets/images/teknik-card-1.png') }}" alt="" class="img card-image">
          <div class="card-text">
            <h4 class="card-text-title">Steaming</h4>
            <p class="card-text-desc">Teknik ecoprint yang dilakukan dengan mengukus daun, bunga, atau bagian tumbuhan
              lain di atas kain yang telah direndam
              dalam larutan mordant.</p>
            <a href="{{ route('viewTechnique') }}" class="btn btn-link">Baca lebih lanjut <svg xmlns="http://www.w3.org/2000/svg" width="24"
                height="24" viewBox="0 0 24 24" fill="none">
                <path
                  d="M14.43 18.8201C14.24 18.8201 14.05 18.7501 13.9 18.6001C13.61 18.3101 13.61 17.8301 13.9 17.5401L19.44 12.0001L13.9 6.46012C13.61 6.17012 13.61 5.69012 13.9 5.40012C14.19 5.11012 14.67 5.11012 14.96 5.40012L21.03 11.4701C21.32 11.7601 21.32 12.2401 21.03 12.5301L14.96 18.6001C14.81 18.7501 14.62 18.8201 14.43 18.8201Z"
                  fill="#887709" />
                <path
                  d="M20.33 12.75H3.5C3.09 12.75 2.75 12.41 2.75 12C2.75 11.59 3.09 11.25 3.5 11.25H20.33C20.74 11.25 21.08 11.59 21.08 12C21.08 12.41 20.74 12.75 20.33 12.75Z"
                  fill="#887709" />
              </svg></a>
          </div>
        </div>
        <div class="card">
          <img src="{{ asset('assets/images/teknik-card-1.png') }}" alt="" class="img card-image">
          <div class="card-text">
            <h4 class="card-text-title">Fermentasi</h4>
            <p class="card-text-desc">Teknik ecoprint yang dilakukan dengan membiarkan daun, bunga, atau bagian tumbuhan
              lain terfermentasi selama beberapa
              hari.</p>
            <a href="{{ route('viewTechnique') }}" class="btn btn-link">Baca lebih lanjut <svg xmlns="http://www.w3.org/2000/svg" width="24"
                height="24" viewBox="0 0 24 24" fill="none">
                <path
                  d="M14.43 18.8201C14.24 18.8201 14.05 18.7501 13.9 18.6001C13.61 18.3101 13.61 17.8301 13.9 17.5401L19.44 12.0001L13.9 6.46012C13.61 6.17012 13.61 5.69012 13.9 5.40012C14.19 5.11012 14.67 5.11012 14.96 5.40012L21.03 11.4701C21.32 11.7601 21.32 12.2401 21.03 12.5301L14.96 18.6001C14.81 18.7501 14.62 18.8201 14.43 18.8201Z"
                  fill="#887709" />
                <path
                  d="M20.33 12.75H3.5C3.09 12.75 2.75 12.41 2.75 12C2.75 11.59 3.09 11.25 3.5 11.25H20.33C20.74 11.25 21.08 11.59 21.08 12C21.08 12.41 20.74 12.75 20.33 12.75Z"
                  fill="#887709" />
              </svg></a>
          </div>
        </div>
        <div class="card">
          <img src="{{ asset('assets/images/teknik-card-1.png') }}" alt="" class="img card-image">
          <div class="card-text">
            <h4 class="card-text-title">Iron Blanket</h4>
            <p class="card-text-desc">Teknik ini dilakukan dengan meletakkan daun, bunga, atau bagian tumbuhan lain di
              atas kain yang telah direndam dalam
              larutan mordant.</p>
            <a href="{{ route('viewTechnique') }}" class="btn btn-link">Baca lebih lanjut <svg xmlns="http://www.w3.org/2000/svg" width="24"
                height="24" viewBox="0 0 24 24" fill="none">
                <path
                  d="M14.43 18.8201C14.24 18.8201 14.05 18.7501 13.9 18.6001C13.61 18.3101 13.61 17.8301 13.9 17.5401L19.44 12.0001L13.9 6.46012C13.61 6.17012 13.61 5.69012 13.9 5.40012C14.19 5.11012 14.67 5.11012 14.96 5.40012L21.03 11.4701C21.32 11.7601 21.32 12.2401 21.03 12.5301L14.96 18.6001C14.81 18.7501 14.62 18.8201 14.43 18.8201Z"
                  fill="#887709" />
                <path
                  d="M20.33 12.75H3.5C3.09 12.75 2.75 12.41 2.75 12C2.75 11.59 3.09 11.25 3.5 11.25H20.33C20.74 11.25 21.08 11.59 21.08 12C21.08 12.41 20.74 12.75 20.33 12.75Z"
                  fill="#887709" />
              </svg></a>
          </div>
        </div>
        <div class="card">
          <img src="{{ asset('assets/images/teknik-card-1.png') }}" alt="" class="img card-image">
          <div class="card-text">
            <h4 class="card-text-title">Bundle Dyeing</h4>
            <p class="card-text-desc">Teknik ecoprint yang dilakukan dengan mengikat kain dengan bahan tumbuhan
              menggunakan tali atau benang.</p>
            <a href="{{ route('viewTechnique') }}" class="btn btn-link">Baca lebih lanjut <svg xmlns="http://www.w3.org/2000/svg" width="24"
                height="24" viewBox="0 0 24 24" fill="none">
                <path
                  d="M14.43 18.8201C14.24 18.8201 14.05 18.7501 13.9 18.6001C13.61 18.3101 13.61 17.8301 13.9 17.5401L19.44 12.0001L13.9 6.46012C13.61 6.17012 13.61 5.69012 13.9 5.40012C14.19 5.11012 14.67 5.11012 14.96 5.40012L21.03 11.4701C21.32 11.7601 21.32 12.2401 21.03 12.5301L14.96 18.6001C14.81 18.7501 14.62 18.8201 14.43 18.8201Z"
                  fill="#887709" />
                <path
                  d="M20.33 12.75H3.5C3.09 12.75 2.75 12.41 2.75 12C2.75 11.59 3.09 11.25 3.5 11.25H20.33C20.74 11.25 21.08 11.59 21.08 12C21.08 12.41 20.74 12.75 20.33 12.75Z"
                  fill="#887709" />
              </svg></a>
          </div>
        </div>
        <div class="card">
          <img src="{{ asset('assets/images/teknik-card-1.png') }}" alt="" class="img card-image">
          <div class="card-text">
            <h4 class="card-text-title">Shibori</h4>
            <p class="card-text-desc">teknik ecoprint yang dilakukan dengan melipat dan mengikat kain untuk menciptakan pola sebelum diwarnai.</p>
            <a href="{{ route('viewTechnique') }}" class="btn btn-link">Baca lebih lanjut <svg xmlns="http://www.w3.org/2000/svg" width="24"
                height="24" viewBox="0 0 24 24" fill="none">
                <path
                  d="M14.43 18.8201C14.24 18.8201 14.05 18.7501 13.9 18.6001C13.61 18.3101 13.61 17.8301 13.9 17.5401L19.44 12.0001L13.9 6.46012C13.61 6.17012 13.61 5.69012 13.9 5.40012C14.19 5.11012 14.67 5.11012 14.96 5.40012L21.03 11.4701C21.32 11.7601 21.32 12.2401 21.03 12.5301L14.96 18.6001C14.81 18.7501 14.62 18.8201 14.43 18.8201Z"
                  fill="#887709" />
                <path
                  d="M20.33 12.75H3.5C3.09 12.75 2.75 12.41 2.75 12C2.75 11.59 3.09 11.25 3.5 11.25H20.33C20.74 11.25 21.08 11.59 21.08 12C21.08 12.41 20.74 12.75 20.33 12.75Z"
                  fill="#887709" />
              </svg></a>
          </div>
        </div>
      </div>
    </div>
  </section>
  <!-- section teknik end -->

  <!-- section katalog -->
  <section class="section-katalog">
    <div class="container container-katalog content">
      <div class="content-top subtext">
        <div class="subtext-text" data-aos="fade-left">
          <span class="subtext-text-title">Ecoprint Elegance:</span>
          <h2 class="subtext-text-display">Jelajahi <span class="text-third">Katalog Produk</span> Kami untuk Gaya Ramah
            Lingkungan yang Memukau!</h2>
        </div>
        <div class="sebtext-text" data-aos="fade-right">
          <a href="{{ route('product.showMain') }}" class="btn btn-link">Lihat lebih lanjut <svg xmlns="http://www.w3.org/2000/svg" width="24"
              height="24" viewBox="0 0 24 24" fill="none">
              <path
                d="M14.43 18.8201C14.24 18.8201 14.05 18.7501 13.9 18.6001C13.61 18.3101 13.61 17.8301 13.9 17.5401L19.44 12.0001L13.9 6.46012C13.61 6.17012 13.61 5.69012 13.9 5.40012C14.19 5.11012 14.67 5.11012 14.96 5.40012L21.03 11.4701C21.32 11.7601 21.32 12.2401 21.03 12.5301L14.96 18.6001C14.81 18.7501 14.62 18.8201 14.43 18.8201Z"
                fill="#887709" />
              <path
                d="M20.33 12.75H3.5C3.09 12.75 2.75 12.41 2.75 12C2.75 11.59 3.09 11.25 3.5 11.25H20.33C20.74 11.25 21.08 11.59 21.08 12C21.08 12.41 20.74 12.75 20.33 12.75Z"
                fill="#887709" />
            </svg></a>
        </div>
      </div>
      <div class="content-bottom" data-aos="fade-up">
        @foreach ($catalog_products as $catalog_product)
        <div class="card">
          <img src="{{ url($catalog_product->image) }}" alt="" class="img card-image">
          <div class="card-text">
            <div class="wrap">
              <h5 class="card-text-title"><a href="{{ route('product.showDetail', ['product' => $catalog_product->id]) }}" style="color: black">{{ $catalog_product->name_produk }}</a></h5>
              @if ($catalog_product->category)
                <span class="subtitle">{{ $catalog_product->category->name_category }}</span>
              @else
                <span class="subtitle">Uncategorized</span>
              @endif
            </div>
            <p class="card-text-price">Rp.{{ number_format($catalog_product->product_price) }}</p>
          </div>
        </div>
        @endforeach
      </div>
    </div>
  </section>
  <!-- section katalog end -->

  <!-- section galeri -->
  <section class="section-galeri">
    <div class="container container-galeri content">
      <div class="content-top subtext" data-aos="fade-up">
        <span class="subtext-title">Ecoprint Essence :</span>
        <h2 class="subtext-display">Sneak Peek ke dalam <span class="text-third">Galeri Produk</span> Kami yang Penuh
          Keelokan dan Keberlanjutan!</h2>
      </div>
      <div class="content-bottom">
        <div class="wrap">
          <div class="item" data-aos="zoom-in">
            <img src="{{ asset('assets/images/galeri-img-1.png') }}" alt="" class="img">
          </div>
          <div class="item" data-aos="zoom-in">
            <img src="{{ asset('assets/images/galeri-img-2.png') }}" alt="" class="img">
          </div>
        </div>
        <div class="wrap">
          <div class="item" data-aos="zoom-in">
            <img src="{{ asset('assets/images/galeri-img-3.png') }}" alt="" class="img">
          </div>
          <div class="item" data-aos="zoom-in">
            <img src="{{ asset('assets/images/galeri-img-4.png') }}" alt="" class="img">
          </div>
          <div class="item" data-aos="zoom-in">
            <img src="{{ asset('assets/images/galeri-img-5.png') }}" alt="" class="img">
          </div>
        </div>
      </div>
    </div>
  </section>
  <!-- section galeri end -->

  <!-- section kesimpulan -->
  <section class="section-kesimpulan">
    <div class="container subtext" data-aos="zoom-in">
      <img src="{{ asset('assets/images/kesimpulan-pattern.png') }}" alt="" class="img img-pattern">
      <h3 class="subtext-display">Ecoprint adalah cara untuk menghubungkan diri dengan alam dan menciptakan sesuatu yang
        indah
        - Katie Jones</h3>
      <img src="{{ asset('assets/images/kesimpulan-pattern.png') }}" alt="" class="img img-pattern">
    </div>
  </section>
  <!-- section kesimpulan end -->

@endsection