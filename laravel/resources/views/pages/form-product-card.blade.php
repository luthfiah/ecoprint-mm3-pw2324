@extends('mainpage')
@section('content')

<section class="section-populer product-card-page">
    <div class="container content">
        <div class="content-top">
            <h3 class="title">Produk Toko: {{ $shop->name_shop }}</h3>
            <a href="{{ route('viewProduct.formAddProduct') }}" class="btn btn-primary">Tambah</a>
        </div>
        <div class="content-bottom">
          @forelse($products as $product)
            <div class="card">
                <img src="{{ asset($product->image) }}" alt="" class="img card-image">
                <div class="card-body">
                  @if($product->category)
                    <span class="subtitle">{{ $product->category->name_category }}</span>
                  @else
                      <span class="subtitle">Uncategorized</span>
                  @endif
                    <h4 class="title">{{ $product->name_produk }}</h4>
                    <div class="rating">
                      <div class="wrap-star">
                        @php
                            $rating = $product->rating;
                            $fullStars = floor($rating);
                            $halfStar = ceil($rating - $fullStars);
                            $emptyStars = 5 - $fullStars - $halfStar;
                        @endphp
                    
                        {{-- Full stars --}}
                        @for ($i = 0; $i < $fullStars; $i++)
                            <img src="{{ asset('assets/images/ic_star-shop.png') }}" alt="" class="ic">
                        @endfor
                    
                        {{-- Half star --}}
                        @if ($halfStar)
                            <img src="{{ asset('assets/images/ic_star-shop.png') }}" alt="" class="ic">
                        @endif
                    
                        {{-- Empty stars --}}
                        @for ($i = 0; $i < $emptyStars; $i++)
                            <img src="{{ asset('assets/images/ic_star-shop.png') }}" alt="" class="ic">
                        @endfor
                      </div>
                      <span>({{ $product->rating }})</span>
                    </div>
                    <span class="subtitle">{{ $product->shop->name_shop }}</span>
                    <div class="price">
                      <h4 class="price-text">Rp.{{ number_format($product->product_price) }}</h4>
                    </div>
                    <div class="wrap-btn d-flex flex-column">
                        <a href="{{ route('product.showDetail', ['product' => $product->id]) }}" class="btn btn-secondary">view</a>
                        <a href="{{ route('viewProduct.editProduct', ['product' => $product->id]) }}" class="btn btn-secondary">edit</a>
                        <form action="{{ route('product.destroy',['product'=>$product->id]) }}" method="POST"
                          class="">
                          @method('DELETE')
                          @csrf
                        <button type="submit" class="btn btn-secondary w-100">Delete</button>
                      </form>
                    </div>
                </div>
            </div>
          @empty
            <p>No products available for this shop.</p>
          @endforelse

        </div>
    </div>
</section>

@endsection
