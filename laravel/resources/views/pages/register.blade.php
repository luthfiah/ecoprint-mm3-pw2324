@extends('mainpage')
@section('content')
  <section class="section-register">
    <div class="container container-register">
      <div class="subtext">
        <h4 class="subtext-display">Daftar akun</h4>
        @if(session('pesan'))
          <div class="alert alert-success w-100">
            {{ session('pesan') }}
          </div>
        @endif

        <form action="{{ route('register.registerPost') }}" method="POST">
          @csrf
          <div class="subtext-input">
            <h6 class="subtext-input-title">Nama Lengkap</h6>
            <input type="text" placeholder="Nama Lengkap" name="name">
            <span class="span-text">Masukkan nama asli Anda!</span>
            @error('name')
              <div class="text-danger">{{ $message }}</div>
            @enderror
          </div>
          <div class="subtext-input">
              <h6 class="subtext-input-title">Email</h6>
              <input type="text" placeholder="Alamat E-mail" name="email">
              <span class="span-text">Gunakan alamat email aktif Anda!</span>
              @error('email')
                <div class="text-danger">{{ $message }}</div>
              @enderror
          </div>
          <div class="subtext-input">
            <h6 class="subtext-input-title">Role</h6>
            <select name="role" class="options">
                <option value="buyer">Buyer</option>
                <option value="seller">Seller</option>
            </select>
            <span class="span-text">Pilih role Anda!</span>
            @error('email')
              <div class="text-danger">{{ $message }}</div>
            @enderror
        </div>
          <div class="subtext-input">
              <h6 class="subtext-input-title">Password</h6>
              <input type="password" placeholder="Masukkan Password" name="password">
              <span class="span-text">Gunakan minimal 8 karakter</span>
              @error('password')
                <div class="text-danger">{{ $message }}</div>
              @enderror
          </div>
          <div class="wrap-button">
              <button type="submit" class="btn btn-primary">Daftar</button>
          </div>
        </form>
        <span class="text-line">atau</span>
        {{-- <div class="wrap-button">
          <button class="btn btn-outline"><img src="{{ asset('assets/images/ic-google.svg') }}" alt="">Masuk dengan
            Google</button>
        </div> --}}
        <p class="text-option">Sudah punya akun? <a href="{{ url('/login') }}" class="text-primary">Masuk Sekarang</a></p>
      </div>
    </div>
  </section>
@endsection