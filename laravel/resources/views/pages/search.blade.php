@extends('mainpage')
@section('content')

    <!-- search -->
    <section class="section-search">
        <div class="container">
            <div class="wrap-desc">
                <img src="{{ asset('assets/images/ic_resources.png') }}" alt="" class="ic">
                <span>Telusuri semua kategori</span>
            </div>
            <div class="wrap-chart">
                <form action="{{ route('product.search') }}" method="GET">
                    <input type="text" placeholder="Cari..." name="query">
                    <button type="submit" class="btn btn-search"><img src="{{ asset('assets/images/ic_search.png') }}" alt="" class="ic"></button>
                </form>
            </div>
        </div>
    </section>

    <section class="section-populer product-card-page">
        <div class="container content">
            <h3>Search Results for "{{ $query }}"</h3>
            @if($results->isEmpty())
                <p>No results found.</p>
            @else
                <div class="content-bottom">
                    @foreach($results as $result)
                        <div class="card">
                            <img src="{{ url($result->image) }}" alt="" class="img card-image">
                            <div class="card-body">
                                @if ($result->category)
                                    <span class="subtitle">{{ $result->category->name_category }}</span>
                                @else
                                    <span class="subtitle">Uncategorized</span>
                                @endif
                                <h4 class="title"><a href="{{ route('product.showDetail', ['product' => $result->id]) }}" style="color: black">{{ $result->name_produk }}</a></h4>
                                <div class="rating">
                                    <div class="wrap-star">
                                        @php
                                            $rating = $result->rating;
                                            $fullStars = floor($rating);
                                            $halfStar = ceil($rating - $fullStars);
                                            $emptyStars = 5 - $fullStars - $halfStar;
                                        @endphp
                                    
                                        {{-- Full stars --}}
                                        @for ($i = 0; $i < $fullStars; $i++)
                                            <img src="{{ asset('assets/images/ic_star-shop.png') }}" alt="" class="ic">
                                        @endfor
                                    
                                        {{-- Half star --}}
                                        @if ($halfStar)
                                            <img src="{{ asset('assets/images/ic_star-shop.png') }}" alt="" class="ic">
                                        @endif
                                    
                                        {{-- Empty stars --}}
                                        @for ($i = 0; $i < $emptyStars; $i++)
                                            <img src="{{ asset('assets/images/ic_star-shop.png') }}" alt="" class="ic">
                                        @endfor
                                    </div>
                                    <span>({{ $result->rating }})</span>
                                </div>
                                <span class="subtitle">{{ $result->shop->name_shop }}</span>
                                <div class="price">
                                    <h4 class="price-text">Rp.{{ number_format($result->product_price) }}</h4>
                                    <a href="{{ $result->shop->phone_number }}" class="btn btn-price">Pesan</a>
                                </div>
                            </div>
                        </div>
                    @endforeach
                </div>
            @endif
        </div>
    </section>
    <!-- search end-->

@endsection