@extends('mainpage')
@section('content')

  <!-- search -->
  <section class="section-search">
    <div class="container">
      <div class="wrap-desc">
        <img src="{{ asset('assets/images/ic_resources.png') }}" alt="" class="ic">
        <span>Telusuri semua kategori</span>
      </div>
      <div class="wrap-chart">
        <form action="{{ route('product.search') }}" method="GET">
          <input type="text" placeholder="Cari..." name="query">
          <button type="submit" class="btn btn-search"><img src="{{ asset('assets/images/ic_search.png') }}" alt="" class="ic"></button>
        </form>
      </div>
    </div>
  </section>
  <!-- search end-->

  <!-- section shop-1 -->
  <section class="header-shop-1 ">
    <div class="container content">
      <div class="content-top">
        <img src="{{ asset('assets/images/shop_header-2.png') }}" alt="" class="img">
        <img src="{{ asset('assets/images/shop_header-2.png') }}" alt="" class="img">
      </div>
      <div class="content-bottom">
        @foreach($categories as $category)
          <a href="{{ route('category.show', ['category' => $category->id]) }}" class="item-link">
            <img src="{{ asset($category->image) }}" alt="" class="img">
            {{ $category->name_category }}
          </a>
        @endforeach
      </div>
    </div>
  </section>
  <!-- section shop-1 end -->

  <!-- section populer -->
  <section class="section-populer">
    <div class="container content">
      <div class="content-top">
        <h3 class="title">Produk Populer</h3>
      </div>
      <div class="content-bottom">
        @foreach ($populer_products as $populer_product)
        <div class="card">
          <img src="{{ url($populer_product->image) }}" alt="" class="img card-image">
          <div class="card-body">
            @if ($populer_product->category)
              <span class="subtitle">{{ $populer_product->category->name_category }}</span>
            @else
              <span class="subtitle">Uncategorized</span>
            @endif
            <h4 class="title"><a href="{{ route('product.showDetail', ['product' => $populer_product->id]) }}" style="color: black">{{ $populer_product->name_produk }}</a></h4>
            <div class="rating">
              <div class="wrap-star">
                @php
                    $rating = $populer_product->rating;
                    $fullStars = floor($rating);
                    $halfStar = ceil($rating - $fullStars);
                    $emptyStars = 5 - $fullStars - $halfStar;
                @endphp
            
                {{-- Full stars --}}
                @for ($i = 0; $i < $fullStars; $i++)
                    <img src="{{ asset('assets/images/ic_star-shop.png') }}" alt="" class="ic">
                @endfor
            
                {{-- Half star --}}
                @if ($halfStar)
                    <img src="{{ asset('assets/images/ic_star-shop.png') }}" alt="" class="ic">
                @endif
            
                {{-- Empty stars --}}
                @for ($i = 0; $i < $emptyStars; $i++)
                    <img src="{{ asset('assets/images/ic_star-shop.png') }}" alt="" class="ic">
                @endfor
              </div>
              <span>({{ $populer_product->rating }})</span>
            </div>
            <span class="subtitle">{{ $populer_product->shop->name_shop }}</span>
            <div class="price">
              <h4 class="price-text">Rp.{{ number_format($populer_product->product_price) }}</h4>
              <a href="{{ $populer_product->shop->phone_number }}" class="btn btn-price">Pesan</a>
              
            </div>
          </div>
        </div>
        @endforeach
      </div>
    </div>
  </section>
  <!-- section populer end -->

  <!-- section terlaris -->
  <section class="section-terlaris">
    <div class="container content">
      <div class="content-top">
        <h3 class="title">Terlaris Setiap Hari</h3>
      </div>
      <div class="content-bottom">
        <div class="item">
          <img src="{{ asset('assets/images/shop_ecology-bag.png') }}" alt="" class="img item-image">
          <h4 class="item-title"><span class="text-secondary">Warnai</span> Hidup Anda,
            Lindungi <span class="text-secondary">Bumi</span> dengan
            <span class="text-secondary">Ecoprint</span>.
          </h4>
        </div>
        @foreach ($best_seller_products as $best_seller_product)
        <div class="card">
          <img src="{{ url($best_seller_product->image) }}" alt="" class="card-image">
          <div class="card-body">
            @if ($best_seller_product->category)
              <span class="subtitle">{{ $best_seller_product->category->name_category }}</span>
            @else
              <span class="subtitle">Uncategorized</span>
            @endif
            <h4 class="title"><a href="{{ route('product.showDetail', ['product' => $best_seller_product->id]) }}" style="color: black">{{ $best_seller_product->name_produk }}</a></h4>
            <div class="rating">
              <div class="wrap-star">
                @php
                    $rating = $best_seller_product->rating;
                    $fullStars = floor($rating);
                    $halfStar = ceil($rating - $fullStars);
                    $emptyStars = 5 - $fullStars - $halfStar;
                @endphp
            
                {{-- Full stars --}}
                @for ($i = 0; $i < $fullStars; $i++)
                    <img src="{{ asset('assets/images/ic_star-shop.png') }}" alt="" class="ic">
                @endfor
            
                {{-- Half star --}}
                @if ($halfStar)
                    <img src="{{ asset('assets/images/ic_star-shop.png') }}" alt="" class="ic">
                @endif
            
                {{-- Empty stars --}}
                @for ($i = 0; $i < $emptyStars; $i++)
                    <img src="{{ asset('assets/images/ic_star-shop.png') }}" alt="" class="ic">
                @endfor
              </div>
              <span>({{ $best_seller_product->rating }})</span>
            </div>
            <span class="subtitle">{{ $best_seller_product->shop->name_shop }}</span>
            <div class="price">
              <h4 class="price-text">Rp.{{ number_format($best_seller_product->product_price) }}</h4>
              <span>{{ $best_seller_product->sold }} terjual</span>
            </div>
            <a href="{{ $best_seller_product->shop->phone_number }}" class="btn btn-price">Pesan</a>
          </div>
        </div>
        @endforeach
      </div>
    </div>
  </section>
  <!-- section terlaris end -->

  <!-- section banner -->
  <section class="section-banner">
    <div class="container">
      <div class="content">
        <div class="content-left subtext">
          <h3 class="subtext-title">Setiap pembelian Anda mendukung <span class="text-secondary">keberlanjutan</span>.
            Warna
            hidup Anda, <span class="text-secondary">lestarikan bumi</span> kita bersama!</h3>
          <span>Beli sekarang di <a href="#" class="text-secondary">Ecoprint!</a></span>
        </div>
        <div class="content-right">
          <img src="{{ asset('assets/images/shop_banner-image.png') }}" alt="" class="img">
        </div>
      </div>
    </div>
  </section>
  <!-- section banner end -->

  <!-- section benefit -->
  <section class="section-benefit">
    <div class="container">
      <div class="item">
        <img src="{{ asset('assets/images/shop_banefit-ic-1.png') }}" alt="" class="img">
        <p class="item-text">
          <span>Harga & penawaran terbaik</span>
          <br>
          Pesan lebih banyak
        </p>
      </div>
      <div class="item">
        <img src="{{ asset('assets/images/shop_banefit-ic-2.png') }}" alt="" class="img">
        <p class="item-text">
          <span>Harga & penawaran terbaik</span>
          <br>
          Pesan lebih banyak
        </p>
      </div>
      <div class="item">
        <img src="{{ asset('assets/images/shop_banefit-ic-3.png') }}" alt="" class="img">
        <p class="item-text">
          <span>Harga & penawaran terbaik</span>
          <br>
          Pesan lebih banyak
        </p>
      </div>
      <div class="item">
        <img src="{{ asset('assets/images/shop_banefit-ic-4.png') }}" alt="" class="img">
        <p class="item-text">
          <span>Harga & penawaran terbaik</span>
          <br>
          Pesan lebih banyak
        </p>
      </div>
      <div class="item">
        <img src="{{ asset('assets/images/shop_banefit-ic-5.png') }}" alt="" class="img">
        <p class="item-text">
          <span>Harga & penawaran terbaik</span>
          <br>
          Pesan lebih banyak
        </p>
      </div>
    </div>
  </section>
  <!-- section benefit end -->

  @endsection