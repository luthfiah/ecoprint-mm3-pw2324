@extends('mainpage')
@section('content')

<!-- input product -->
<section class="section-input-product">
    <div class="container flex-column w-100 justify-content-center align-items-center">
        <h1 class="input-product-title">Edit Category</h1>
        @if(Session::has('success'))
        <div class="alert alert-success w-100" role="alert">
            {{ Session::get('success') }}
        </div>
        @endif
        <form action="{{ route('viewCategory.editCategory', ['category' => $category->id]) }}" method="POST">
            @csrf
            @method('PATCH')
            <div class="wrap-input-product">
                <label for="fname">Nama Category</label>
                <input type="text" id="fname" placeholder="Masukan nama category" name="fname"
                    value="{{ old('fname') ?? $category->name_category }}">
                @error('fname')
                <div class="text-danger">{{ $message }}</div>
                @enderror
            </div>
            <div class="subtext-input">
                <h6 class="subtext-input-title">Foto Kategori</h6>
                <img height="150px" src="{{url('')}}/{{$category->image}}" class="rounded" alt="">
                <input type="file" name="image">
                <span class="span-text">Pilih foto kategori Anda</span>
                @error('image')
                    <div  class="text-danger">{{ $message }}</div>
                @enderror
            </div>
            <input type="submit" class="btn btn-primary">
        </form>
    </div>
</section>
<!-- input product -->

@endsection
