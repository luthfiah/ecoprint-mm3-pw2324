@extends('mainpage')
@section('content')

  <!-- input product -->
  <section class="section-input-product">
    <div class="container flex-column w-100 justify-content-center align-items-center">
      <h1 class="input-product-title">Tambah Toko</h1>
      @if(Session::has('success'))
        <div class="alert alert-success w-100" role="alert">
        {{Session::get('success')}}
        </div>
      @endif
      <form action="{{route('viewShop.addShop')}}" method="POST">
        @csrf
        <div class="wrap-input-product">
          <label for="">Nama Toko</label>
          <input type="text" id="fname" placeholder="Masukan nama produk" name="fname">
          @error('fname')
            <div  class="text-danger">{{ $message }}</div>
          @enderror
        </div>
        <div class="wrap-input-product">
          <label for="">Link Whatsapp</label>
          <input type="text" id="fphone" placeholder="Masukan link whatsapp" name="fphone">
          @error('fphone')
            <div  class="text-danger">{{ $message }}</div>
          @enderror
        </div>
        <input type="submit" class="btn btn-primary">
      </form>
    </div>
  </section>
  <!-- input product -->

  @endsection