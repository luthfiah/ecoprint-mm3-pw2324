<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="icon" type="image/x-icon" href="{{asset('assets/images/ic-logo.png')}}">
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.3.2/dist/css/bootstrap.min.css" rel="stylesheet"
        integrity="sha384-T3c6CoIi6uLrA9TneNEoa7RxnatzjcDSCmG1MXxSR1GAsXEV/Dwwykc2MPK8M2HN" crossorigin="anonymous">
    <link href="https://unpkg.com/aos@2.3.1/dist/aos.css" rel="stylesheet" />
    <link rel="stylesheet" href="{{asset('assets/css/main.css')}}">
    <title>Eco Print</title>
    </head>

    <body>
    <!-- navbar -->
    <nav class="navbar">
        <div class="container container-nav nav">
            <a href="{{ route('product.showHome') }}" class="nav-logo">
                <img src="{{asset('assets/images/ic-logo.png')}}" alt="" class="ic">
            </a>
            <div class="nav-menu">
                <a href="{{ route('product.showHome') }}" class="{{ Request::is('/') ? 'active' : '' }} nav-link">Beranda</a>
                <div class="btn-group">
                    <button type="button" class="nav-link dropdown-toggle" data-bs-toggle="dropdown" aria-expanded="false">
                        Edukasi
                    </button>
                    <ul class="dropdown-menu">
                        <li><a class="{{ Request::is('/about-benefit-ecoprint') ? 'active' : '' }} dropdown-item" href="{{ route('viewBenefit') }}">Tentang Ecoprint & Manfaat</a></li>
                        <li><a class="{{ Request::is('/about-ecoprint') ? 'active' : '' }} dropdown-item" href="{{ route('viewTechnique') }}">Teknik Ecoprint</a></li>
                    </ul>
                </div>
                <a href="{{ route('product.showMain') }}" class="{{ Request::is('/') ? 'active' : '' }} nav-link">Berbelanja</a>

                @auth
                    @if(auth()->user()->role === 'admin')
                        <div class="btn-group">
                            <button type="button" class="nav-link dropdown-toggle" data-bs-toggle="dropdown" aria-expanded="false">
                                Admin
                            </button>
                            <ul class="dropdown-menu">
                                <li><a class="{{ Request::is('/shop') ? 'active' : '' }} dropdown-item" href="{{ route('viewShop') }}">Shop</a></li>
                                <li><a class="{{ Request::is('/') ? 'active' : '' }} dropdown-item" href="{{ route('viewCategory') }}">Category</a></li>
                            </ul>
                        </div>
                    @elseif(auth()->user()->role === 'seller')
                        <a class="{{ Request::is('/shop') ? 'active' : '' }} nav-link" href="{{ route('viewShop') }}">Shop</a>
                    @endif
                @endauth

            </div>
            <div class="nav-buttons">
                @if (!Auth::user())
                    <a href="{{ route('login.index') }}" class="btn btn-primary btn-primary-custom">Masuk</a>
                    <a href="{{ route('register') }}" class="btn btn-secondary">Daftar</a>
                @else
                    <form action="{{ route('logout.logout') }}" method="POST">
                        @csrf
                        <li>
                            <button type="submit" class="btn menu-link btn-secondary" id="login">
                                <i class="menu-icon tf-icons bx bx-log-out"></i>
                                <div>Logout</div>
                            </button>
                        </li>
                    </form>
                @endif
                <button class="nav-btn-responsive"></button>
            </div>
            
        </div>
    </nav>
    
    <!-- navbar end -->

    <!-- main -->
    <main>
        @yield('content')
    </main>

    <!-- footer -->
    <footer class="footer">
        <div class="container content">
        <div class="content-top">
            <a href="{{ route('product.showHome') }}" class="logo">
            <img src="{{asset('assets/images/ic-logo.png')}}" alt="" class="ic">
            </a>
            <div class="subtext">
            <h5 class="subtext-title">Informasi</h5>
            <div class="subtext-text">
                <a href="{{ route('product.showHome') }}" class="nav-link">Beranda</a>
                <a href="{{ route('viewBenefit') }}" class="nav-link">Edukasi</a>
                <a href="{{ route('viewBenefit') }}" class="nav-link">Manfaat</a>
                <a href="{{ route('viewTechnique') }}" class="nav-link">Teknik</a>
                <a href="{{ route('product.showMain') }}" class="nav-link">Berbelanja</a>
            </div>
            </div>
            <div class="subtext">
            <h5 class="subtext-title">Kontak</h5>
            <div class="subtext-text">
                <a href="#" class="nav-link"><img src="{{asset('assets/images/ic_email.svg')}}" alt="" class="ic"> <span>ecoprintPPL@gmail.com</span></a>
                <a href="#" class="nav-link"><img src="{{asset('assets/images/ic_whatsapp.svg')}}" alt="" class="ic">
                <span>0822-7891-5251</span></a>
                <a href="#" class="nav-link"><img src="{{asset('assets/images/ic_home.svg')}}" alt="" class="ic">
                <span>Jl.Pelajar Pejuang 123 Majalaya Purwokerto Indonesia</span></a>
            </div>
            </div>
            <div class="subtext">
            <h5 class="subtext-title">Social Media</h5>
            <div class="subtext-text social-media">
                <a href="#" class="nav-link"><img src="{{asset('assets/images/ic_facebook.svg')}}" alt="" class="ic"></a>
                <a href="#" class="nav-link"><img src="{{asset('assets/images/ic_twitter.svg')}}" alt="" class="ic"></a>
                <a href="#" class="nav-link"><img src="{{asset('assets/images/ic_linkedin.svg')}}" alt="" class="ic"></a>
                <a href="#" class="nav-link"><img src="{{asset('assets/images/ic_pininterest.svg')}}" alt="" class="ic"></a>
            </div>
            </div>
        </div>
        <div class="content-bottom">
            <span class="copyright">Copyright © 2023 Kelompok PPL Ecoprint ITTP | All Rights Reserved</span>
        </div>
        </div>
    </footer>
    <!-- footer end -->

    <script src="https://cdn.jsdelivr.net/npm/@popperjs/core@2.11.8/dist/umd/popper.min.js"
        integrity="sha384-I7E8VVD/ismYTF4hNIPjVp/Zjvgyol6VFvRkX/vR+Vc4jQkC+hVqc2pM8ODewa9r"
        crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.3.2/dist/js/bootstrap.min.js"
        integrity="sha384-BBtl+eGJRgqQAUMxJ7pMwbEyER4l1g+O15P+16Ep7Q9Q+zqX6gSbd85u4mG4QzX+"
        crossorigin="anonymous"></script>
    <script src="https://unpkg.com/aos@2.3.1/dist/aos.js"></script>
    <script src="{{asset('assets/js/main.js')}}"></script>
</body>

</html>