<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class category extends Model
{
    use HasFactory;

    public function category(){
        return $this->hasMany(category::class, 'id_categories');
    }

    public function products()
    {
        return $this->hasMany(product::class, 'id_categories');
    }
}
