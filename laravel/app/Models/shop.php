<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class shop extends Model
{
    use HasFactory;

    protected $fillable = [
        'name_shop',
        'phone_number',
        'user_id',
    ];

    // Define the relationship with the User model
    public function user()
    {
        return $this->belongsTo(User::class, 'user_id');
    }
    public function category(){
        return $this->hasMany(Category::class, 'shop_id');
    }
    public function products()
    {
        return $this->hasMany(product::class, 'shop_id');
    }

    public function shop()
    {
        return $this->hasMany(shop::class, 'shop_id');
    }
}
