<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\category;
use File;
use Illuminate\Support\Facades\Auth;
class CategoryController extends Controller
{
    public function index()
    {
        $category = category::all();
        return view('pages.form-category',['categories' => $category]);
    }

    public function addShowCategory()
    {
        return view('pages.form-category-add');
    }

    public function addCategory(Request $request)
    {
        $validateData = $request->validate([
            'fname' => 'required',
            'image' => 'required|image|mimes:jpeg,png,jpg,gif,svg|max:2048',
        ]);

        $category = new category();
        $category->name_category = $validateData['fname'];
        if ($request->hasFile('image')) {
            $extFile = $request->image->getClientOriginalExtension();
            $namaFile = 'user-' . time() . "." . $extFile;
            $path = $request->image->move('assets/images_category', $namaFile);
            $category->image = $path;
        };
        $category->save();

        $request->session()->flash('success', 'Penambahan data berhasil');
        return redirect()->route('viewCategory.formAddCategory');
    }

    public function edit($id)
    {
        $category = category::where('id', $id)->firstOrFail();
        return view('pages.form-category-edit', ['category' => $category]);
    }


    public function editCategory(Request $request, category $category)
    {
        $validateData = $request->validate([
            'fname' => 'required',
            'image' => 'image|mimes:jpeg,png,jpg,gif,svg|max:2048',
        ]);

        $category->name_category = $validateData['fname'];
        if ($request->hasFile('image')) {
            $extFile = $request->image->getClientOriginalExtension();
            $namaFile = 'user-' . time() . "." . $extFile;
            $path = $request->image->move('assets/images_category', $namaFile);
            $category->image = $path;
        }

        $category->save();
        $request->session()->flash('success', 'Perubahan data berhasil');
        return redirect()->route('viewCategory');
    }

    public function destroy(Request $request, category $category)
    {
        // Delete related products
        $category->products()->delete();

        // Delete the shop record
        $category->delete();

        $request->session()->flash('success', 'Hapus data berhasil');
        return redirect()->route('viewCategory');
    }
}
