<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\User;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Auth;
class AuthController extends Controller
{
    public function register(){
        return view('pages.register');
    }

    public function registerPost(Request $request)
    {
        $validateData = $request->validate([
            'name' => 'required',
            'email' => 'required|email|unique:users',
            'role' => 'required',
            'password' => 'required|min:8',
        ]);

        $user = new User();

        $user->name = $validateData['name'];
        $user->email = $validateData['email'];
        $user->password = Hash::make($validateData['password']);

        $user->role = $validateData['role'];

        $user->save();
        $request->session()->flash('pesan', 'Register successfully');
        return redirect()->route('register');
    }
    public function index()
    {
        return view('pages.login');
    }
    public function process(Request $request)
    {
        $request->validate([
            'email' => 'required|email',
            'password' => 'required',
        ]);
    
        $credentials = $request->only('email', 'password');
    
        if (Auth::attempt($credentials)) {
            return redirect()->intended('/');
        } else {
            return back()->withInput()->with('pesan', 'Email atau password salah');
        }
    }

    public function logout(){
        Auth::logout();
        return redirect('login')->with('pesan', 'Logout berhasil');
    }

    // User.php model

}
