<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\product;
use File;
use Illuminate\Support\Facades\Auth;
use App\Models\category;
use App\Models\shop;
class ProductController extends Controller
{
    public function addShowProduct()
    {
        $categories = Category::all();
        $seller_id = Auth::user()->id;
        $shops = shop::where('user_id', $seller_id)->get();

        return view('pages.form-product', compact('categories', 'shops'));
    }

    public function addProduct(Request $request)
    {
        $validateData = $request->validate([
            'fname' => 'required|min:3|max:50|unique:products,name_produk',
            'shop' => 'required',
            'product_price' => 'required|numeric|max:9999999.99',
            'image' => 'required|image|mimes:jpeg,png,jpg,gif,svg|max:2048',
            'detail' => 'required|min:3',
            'sold' => 'required|numeric',
            'rating' => 'required|numeric',
            'category' => 'required',
        ]);

        // Handle image upload
        

        $product = new Product();
        $product->name_produk = $validateData['fname'];
        $product->product_price = $validateData['product_price'];
        $product->detail = $validateData['detail'];
        $product->sold = $validateData['sold'];
        $product->rating = $validateData['rating'];
        $product->id_categories = $validateData['category'];
        $product->shop_id = $validateData['shop'];
        if ($request->hasFile('image')) {
            $extFile = $request->image->getClientOriginalExtension();
            $namaFile = 'user-' . time() . "." . $extFile;
            $path = $request->image->move('assets/images_product', $namaFile);
            $product->image = $path;
        };

        $product->save();

        $request->session()->flash('success', 'Penambahan data berhasil');
        return redirect()->route('viewProduct.formAddProduct');
    }

    public function showProduct($shop_id)
    {
        $category = Category::all();
        $products = Product::where('shop_id', $shop_id)->get();
        $shop = Shop::find($shop_id);

        return view('pages.form-product-card', compact('products', 'shop', 'category'));
    }

    public function editProductForm(product $product)
    {
        $categories = Category::all();
        $seller_id = Auth::user()->id;
        $shops = shop::where('user_id', $seller_id)->get();
        return view('pages.form-product-edit', compact('product', 'categories', 'shops'));
    }

    public function editProduct(Request $request, product $product)
    {
        $validateData = $request->validate([
            'fname' => 'required|min:3|max:50|unique:products,name_produk,' . $product->id,
            'shop' => 'required',
            'product_price' => 'required|numeric|max:9999999.99',
            'image' => 'image|mimes:jpeg,png,jpg,gif,svg|max:2048',
            'detail' => 'required|min:3',
            'sold' => 'required|numeric',
            'rating' => 'required|numeric',
            'category' => 'required',
        ]);

        $product->name_produk = $validateData['fname'];
        $product->product_price = $validateData['product_price'];
        $product->detail = $validateData['detail'];
        $product->sold = $validateData['sold'];
        $product->rating = $validateData['rating'];
        $product->id_categories = $validateData['category'];
        $product->shop_id = $validateData['shop'];

        if ($request->hasFile('image')) {
            $extFile = $request->image->getClientOriginalExtension();
            $namaFile = 'user-' . time() . "." . $extFile;
            $path = $request->image->move('assets/images_product', $namaFile);
            $product->image = $path;
        }

        $product->save();

        $request->session()->flash('success', 'Perubahan data berhasil');
        return redirect()->route('viewProduct.editProductForm', ['product' => $product->id]);
    }

    public function destroy(Request $request, product $product)
    {
        $product->delete();
        $request->session()->flash('success','Hapus data berhasil');
        return redirect()->route('viewProduct', ['shop_id' => $product->shop_id]);
    }

    public function showProductDetail(product $product)
    {
        $categories = Category::all();
        $seller_id = Auth::user()->id;
        $shops = shop::where('user_id', $seller_id)->get();

        $related_products = $this->getRelatedProducts($product->id);



        return view('pages.detail', compact('product', 'categories', 'shops', 'related_products'));
    }

    public function showProductShop(product $product)
    {
        $categories = Category::all();
        $seller_id = Auth::user()->id;
        $shops = shop::where('user_id', $seller_id)->get();

        $populer_products = $this->getPopulerProducts($product->id);
        $best_seller_products = $this->getBestSellerProducts($product->id);

        return view('pages.shop-1', compact('product', 'categories', 'shops', 'populer_products', 'best_seller_products'));
    }
    

    public function showProductHome(product $product)
    {
        $categories = Category::all();

        $catalog_products = $this->getCatalogProducts($product->id);

        return view('pages.index', compact('product', 'categories', 'catalog_products'));
    }

    private function getRelatedProducts($current_product_id)
    {
        return Product::where('id', '!=', $current_product_id)
            ->take(10)
            ->get();
    }

    private function getPopulerProducts($current_product_id)
    {
        return Product::where('id', '!=', $current_product_id)
            ->orderByDesc('rating')
            ->take(10)
            ->get();
    }

    private function getBestSellerProducts($current_product_id)
    {
        return Product::where('id', '!=', $current_product_id)
            ->orderByDesc('sold')
            ->take(3)
            ->get();
    }

    private function getCatalogProducts($current_product_id)
    {
        return Product::where('id', '!=', $current_product_id)
            ->take(4)
            ->get();
    }

    public function search(Request $request)
    {
        $query = $request->input('query');
        $results = Product::where('name_produk', 'like', '%' . $query . '%')->get();
        return view('pages.search', compact('results', 'query'));
    }

    public function categoryShow(category $category)
    {
        $categories = Category::all();

        // Ambil produk berdasarkan ID kategori
        $products = Product::where('id_categories', $category->id)->get();

        return view('pages.shop-2', compact('products', 'categories'));
    }
}
