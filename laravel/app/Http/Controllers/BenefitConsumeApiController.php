<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use GuzzleHttp\Client;
use Exception;
class BenefitConsumeApiController extends Controller
{
    public function index()
    {
        $client = new Client();
        $apiUrl = 'https://greenweave.kelasmm3.cloud/ecoprint-mm3-pw2324/api/benefit';

        $response = $client->request('GET', $apiUrl);
        $benefits = json_decode($response->getBody(), true);

        return view('pages.manfaat', ['benefits' => $benefits]);
    }

    public function about(){
        return view('pages.teknik');
    }
}
