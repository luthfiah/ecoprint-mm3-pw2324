<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use File;
use App\Models\Benefit;
use Illuminate\Support\Facades\Validator;
class BenefitApiController extends Controller
{
    /**
     * Display a listing of the resource.
     */
    public function index()
    {
        $benefits = Benefit::all()->toJson(JSON_PRETTY_PRINT);
        return response($benefits, 200);
    }

    /**
     * Show the form for creating a new resource.
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(Request $request)
    {
        $validateData = Validator::make($request->all(), [
            'title' => 'required|min:3|max:50|unique:benefits,title',
            'image' => 'required|image|mimes:jpeg,png,jpg,gif,svg|max:2048',
            'detail' => 'required|min:3',
        ]);
        if ($validateData->fails()) {
            return response($validateData->errors(), 400);
        } else {
            $benefit = new Benefit();
            $benefit->title =$request->title;
            $benefit->detail = $request->detail;
            if ($request->hasFile('image')) {
                $extFile = $request->image->getClientOriginalExtension();
                $namaFile = 'user-' . time() . "." . $extFile;
                $path = $request->image->move('assets/images_benefit', $namaFile);
                $benefit->image = $path;
            };

            $benefit->save();
            return response()->json([
                "message" => "benefit record created"
            ], 201);
        }
    }

    /**
     * Display the specified resource.
     */
    public function show(string $id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     */
    public function edit(string $id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(Request $request, string $id)
    {
        if (Benefit::where('id', $id)->exists()) {
        $validateData = Validator::make($request->all(), [
            'title' => 'required|min:3|max:50|unique:benefits,title',
            'image' => 'required|image|mimes:jpeg,png,jpg,gif,svg|max:2048',
            'detail' => 'required|min:3',
        ]);
        if ($validateData->fails()) {
            return response($validateData->errors(), 400);
        } else {
            $benefit = Benefit::find($id);
            $benefit->title = $request->title;
            $benefit->detail = $request->detail;
            if ($request->hasFile('image')) {
                $extFile = $request->image->getClientOriginalExtension();
                $namaFile = 'user-' . time() . "." . $extFile;
                File::delete($benefit->image);
                $path = $request->image->move('assets/images', $namaFile);
                $benefit->image = $path;
            }
            $benefit->save();
            return response()->json([
                "message" => "benefit record updated"
            ], 201);
        }
    } else {
        return response()->json([
            "message" => "Benefit not found"
        ], 404);
    }
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy(string $id)
    {
        if (Benefit::where('id', $id)->exists()) {
            $benefit = Benefit::find($id);
            File::delete($benefit->image);
            $benefit->delete();
            return response()->json([
                "message" => "benefit record deleted"
            ], 201);
        } else {
            return response()->json([
                "message" => "Benefit not found"
            ], 404);
        }
    }
}
