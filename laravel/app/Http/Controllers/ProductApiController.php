<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use File;
use App\Models\product;
use Illuminate\Support\Facades\Validator;
class ProductApiController extends Controller
{
    /**
     * Display a listing of the resource.
     */
    public function index()
    {
        $products = product::all()->toJson(JSON_PRETTY_PRINT);
        return response($products, 200);
    }

    /**
     * Show the form for creating a new resource.
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     */
    public function addProduct(Request $request)
    {
        $validateData = Validator::make($request->all(), [
            'name_produk' => 'required|min:3|max:50|unique:products,name_produk',
            'shop_id' => 'required',
            'product_price' => 'required|numeric|max:9999999.99',
            'image' => 'required|image|mimes:jpeg,png,jpg,gif,svg|max:2048',
            'detail' => 'required|min:3',
            'sold' => 'required|numeric',
            'rating' => 'required|numeric',
            'id_categories' => 'required',
        ]);
        if ($validateData->fails()) {
            return response($validateData->errors(), 400);
        } else {
            $product = new Product();
            $product->name_produk =$request->name_produk;
            $product->product_price = $request->product_price;
            $product->detail = $request->detail;
            $product->sold = $request->sold;
            $product->rating = $request->rating;
            $product->id_categories = $request->id_categories;
            $product->shop_id = $request->shop_id;
            if ($request->hasFile('image')) {
                $extFile = $request->image->getClientOriginalExtension();
                $namaFile = 'user-' . time() . "." . $extFile;
                $path = $request->image->move('assets/images_product', $namaFile);
                $product->image = $path;
            };

            $product->save();
            return response()->json([
                "message" => "product record created"
            ], 201);
        }
    }

    /**
     * Display the specified resource.
     */
    public function show(string $id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     */
    public function edit(string $id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(Request $request, string $id)
    {
        if (product::where('id', $id)->exists()) {
            $validateData = Validator::make($request->all(), [
                'name_produk' => 'required|min:3|max:50|unique:products,name_produk',
                'shop_id' => 'required',
                'product_price' => 'required|numeric|max:9999999.99',
                'image' => 'image|mimes:jpeg,png,jpg,gif,svg|max:2048',
                'detail' => 'required|min:3',
                'sold' => 'required|numeric',
                'rating' => 'required|numeric',
                'id_categories' => 'required',
            ]);
            if ($validateData->fails()) {
                return response($validateData->errors(), 400);
            } else {
                $product = product::find($id);
                $product->name_produk = $request->name_produk;
                $product->product_price = $request->product_price;
                $product->detail = $request->detail;
                $product->sold = $request->sold;
                $product->rating = $request->rating;
                $product->id_categories = $request->id_categories;
                $product->shop_id = $request->shop_id;
                if ($request->hasFile('image')) {
                    $extFile = $request->image->getClientOriginalExtension();
                    $namaFile = 'user-' . time() . "." . $extFile;
                    File::delete($product->image);
                    $path = $request->image->move('assets/images', $namaFile);
                    $product->image = $path;
                }
                $product->save();
                return response()->json([
                    "message" => "product record updated"
                ], 201);
            }
        } else {
            return response()->json([
                "message" => "Product not found"
            ], 404);
        }
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy(string $id)
    {
        if (product::where('id', $id)->exists()) {
            $product = product::find($id);
            File::delete($product->image);
            $product->delete();
            return response()->json([
                "message" => "product record deleted"
            ], 201);
        } else {
            return response()->json([
                "message" => "Product not found"
            ], 404);
        }
    }


}
