<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\shop;
use File;
use App\Models\User;
use Illuminate\Support\Facades\Auth;
class ShopController extends Controller
{
    public function form()
    {
        return view('pages.form-shop');
    }

    public function addShop(Request $request){
        $validateData = $request->validate([
            'fname' => 'required|min:3|max:50|unique:shops,name_shop',
            'fphone' => 'required',
        ]);

        $shop = new shop();
        $shop->name_shop = $validateData['fname'];
        $shop->phone_number = $validateData['fphone'];

        $shop->user_id = auth()->user()->id;
        $shop->save();
        $request->session()->flash('success', 'Penambahan data berhasil');
        return redirect()->route('viewShop.formAddShop');
    }

    public function edit($id)
    {
        $shop = shop::where('id', $id)->firstOrFail();
        return view('pages.form-shop-edit', ['shop' => $shop]);
    }


    public function editShop(Request $request, shop $shop){
        $validateData = $request->validate([
            'fname' => 'required|min:3|max:50,unique:shops,name_shop',
            'fphone' => 'required',
        ]);

        $shop->name_shop = $validateData['fname'];
        $shop->phone_number = $validateData['fphone'];

        $shop->save();
        $request->session()->flash('success', 'Perubahan data berhasil');
        return redirect()->route('viewShop');
    }

    public function destroy(Request $request, Shop $shop)
    {
        // Delete related products
        $shop->products()->delete();

        // Delete the shop record
        $shop->delete();

        $request->session()->flash('success', 'Hapus data berhasil');
        return redirect()->route('viewShop');
    }


    public function shop(){
        $seller_id = Auth::user()->id;
        $shops = shop::where('user_id', $seller_id)->get();
        return view('pages.form-toko', compact('shops'));
    }

}
