<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\ShopApiController;
use App\Http\Controllers\CategoryApiController;
use App\Http\Controllers\ProductApiController;
use App\Http\Controllers\BenefitApiController;
/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider and all of them will
| be assigned to the "api" middleware group. Make something great!
|
*/

Route::middleware('auth:sanctum')->get('/user', function (Request $request) {
    return $request->user();
});

Route::post('product-add', [ProductApiController::class,'addProduct']);
Route::get('product', [ProductApiController::class,'index']);
Route::post('product/{id}', [ProductApiController::class, 'update']);
Route::delete('product/{id}', [ProductApiController::class,'destroy']);


Route::get('category', [CategoryApiController::class,'index']);
Route::get('shop', [ShopApiController::class,'index']);



Route::post('benefit-add', [BenefitApiController::class,'store']);
Route::get('benefit', [BenefitApiController::class,'index']);
Route::post('benefit/{id}', [BenefitApiController::class, 'update']);
Route::delete('benefit/{id}', [BenefitApiController::class,'destroy']);