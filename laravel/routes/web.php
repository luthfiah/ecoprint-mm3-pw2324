<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\ShopController;
use App\Http\Controllers\AuthController;
use App\Http\Controllers\CategoryController;
use App\Http\Controllers\ProductController;
use App\Http\Controllers\BenefitConsumeApiController;


/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider and all of them will
| be assigned to the "web" middleware group. Make something great!
|
*/

Route::get('/', function () {
    return view('pages.index');
})->middleware('login_auth');

Route::get('/', [ProductController::class,'showProductHome'])->name('product.showHome');

Route::get('/about-ecoprint', [BenefitConsumeApiController::class, 'about'])->name('viewTechnique');

Route::get('/about-benefit-ecoprint', [BenefitConsumeApiController::class, 'index'])->name('viewBenefit');

Route::get('/shop', [ShopController::class, 'shop'])->name('viewShop')->middleware('seller');

Route::get('/shop-add', [ShopController::class, 'form'])->name('viewShop.formAddShop')->middleware('seller');

Route::post('/shop-add', [ShopController::class, 'addShop'])->name('viewShop.addShop')->middleware('seller');

Route::get('/shop/{shop}/update', [ShopController::class, 'edit'])->name('viewShop.formEditShop')->middleware('seller');

Route::patch('/shop/{shop}/update', [ShopController::class, 'editShop'])->name('viewShop.editShop')->middleware('seller');

Route::delete('/shop/{shop}', [ShopController::class,'destroy'])
->name('shop.destroy')->middleware('seller');

Route::get('/product/{shop_id}', [ProductController::class, 'showProduct'])->name('viewProduct')->middleware('seller');

Route::get('/product-add', [ProductController::class, 'addShowProduct'])->name('viewProduct.formAddProduct')->middleware('seller');

Route::post('/product-add', [ProductController::class, 'addProduct'])->name('viewProduct.addProduct')->middleware('seller');

Route::get('/product/{product}/edit', [ProductController::class, 'editProductForm'])->name('viewProduct.editProductForm')->middleware('seller');

Route::patch('/product/{product}/edit', [ProductController::class, 'editProduct'])->name('viewProduct.editProduct')->middleware('seller');

Route::delete('/product/{product}', [ProductController::class,'destroy'])
->name('product.destroy')->middleware('seller');;





Route::get('/category', [CategoryController::class, 'index'])->name('viewCategory')->middleware('admin');

Route::get('/category-add', [CategoryController::class, 'addShowCategory'])->name('viewCategory.formAddCategory')->middleware('admin');

Route::post('/category-add', [CategoryController::class, 'addCategory'])->name('viewCategory.addCategory')->middleware('admin');

Route::get('/category/{category}/update', [CategoryController::class, 'edit'])->name('viewCategory.formEditCategory')->middleware('admin');

Route::patch('/category/{category}/update', [CategoryController::class, 'editCategory'])->name('viewCategory.editCategory')->middleware('admin');

Route::delete('/category/{category}', [CategoryController::class,'destroy'])
->name('category.destroy')->middleware('admin');


Route::get('/search', [ProductController::class, 'search'])->name('product.search')->middleware('login_auth');

Route::get('/category-show/{category}', [ProductController::class, 'categoryShow'])->name('category.show')->middleware('login_auth');

Route::get('/product/{product}/detail', [ProductController::class,'showProductDetail'])->name('product.showDetail')->middleware('login_auth');

Route::get('/main-shopping', [ProductController::class,'showProductShop'])->name('product.showMain')->middleware('login_auth');




Route::get('/login', [AuthController::class,'index'])->name('login.index')->middleware('afterLogin');
Route::get('/logout', [AuthController::class,'logout'])->name('login.logout')->middleware('afterLogin');
Route::post('/login', [AuthController::class,'process'])->name('login.process')->middleware('afterLogin');

Route::get('/register', [AuthController::class, 'register'])->name('register')->middleware('afterLogin');

Route::post('/register', [AuthController::class, 'registerPost'])->name('register.registerPost')->middleware('afterLogin');

Route::post('/logout', [AuthController::class, 'logout'])->name('logout.logout');